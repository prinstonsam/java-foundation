package com.epam.ex06_07;

@ClassPreamble(
		author = "Igor",
		currentRevision = 1,
		reviewers = {"Igor", "Natalia"}
)
public interface ServiceShip {
	public Ship getShip();
}

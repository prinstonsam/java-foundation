package com.epam.generic;

/**
 * Created by isamsonov on 3/28/16.
 */
public class Mark<T extends Number> {
	public T mark;

	public Mark( T mark ) {
		this.mark = mark;
	}

	public T getMark() {
		return mark;
	}

	public void setMark( T mark ) {
		this.mark = mark;
	}

	public int roundMark() {
		return Math.round( mark.floatValue() );
	}

	public boolean sameAny( Mark<?> ob ) {
		return roundMark() == ob.roundMark();
	}

	public boolean same( Mark<T> obj ) {
		return getMark() == obj.getMark();
	}
}

package com.epam.module01.hw01.ex06;

/**
 * Created by isamsonov on 3/24/16.
 */
public class NoteRecord {
	private String item;

	public NoteRecord() {
	}

	public NoteRecord( String item ) {
		this.item = item;
	}

	public String getItem() {
		return item;
	}

	public void setItem( String item ) {
		this.item = item;
	}

	@Override
	public boolean equals( Object o ) {
		if ( this == o ) return true;
		if ( ! ( o instanceof NoteRecord ) ) return false;

		NoteRecord that = ( NoteRecord ) o;

		return getItem().equals( that.getItem() );

	}

	@Override
	public int hashCode() {
		return getItem().hashCode();
	}

	@Override
	public String toString() {
		return item;
	}
}

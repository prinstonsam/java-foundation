package horstman.internationalization;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by isamsonov on 4/4/16.
 */
public class MessageFormatInspect {
	public static final String patern = "Hi {0}, current date {1,date,full}, current time {2, time, full}";
	public static void main( String[] args ) {


		//with default locale
		String msg = MessageFormat.format( patern, 0, new Date(), new Date() );
		System.out.println( msg );

		//with arbitrary locale
		MessageFormat mf = new MessageFormat( patern, new Locale( "ru", "RU" ) );

		msg = mf.format( new Object[] {0, new Date(), new Date()} );

		System.out.println(msg);

	}

}

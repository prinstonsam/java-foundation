package com.epam.easyclass;

/**
 * Created by samsik on 27.03.16.
 */
class A{
    public void method(Double value) {
        value = Double.valueOf(999);
    }
}

public class Aaaa {
    public static void main(String[] args) {

        A a = new A();

        Double value = 111D;

        a.method(value);

        System.out.println(value);
    }
}

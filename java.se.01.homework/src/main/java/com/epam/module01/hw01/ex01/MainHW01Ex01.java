package com.epam.module01.hw01.ex01;

/**
 * Необходимо скомпилировать и запустить данный код из консоли
 *
 * java -cp . com.epam.homework01.Main
 */
public class MainHW01Ex01{
    public static void main(String[] args) {
        Logic logic = new Logic();
        System.out.println(logic.method());
    }
}

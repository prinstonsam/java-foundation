package com.epam.generic;

/**
 * Created by isamsonov on 3/28/16.
 */
public class MarkInspector {
	public static void main( String[] args ) {
		Mark<Double> doubleMark = new Mark<>( 71.4D );
		System.out.println( doubleMark.sameAny( doubleMark ) );
		Mark<Integer> integerMark = new Mark<>( 71 );
		System.out.println( doubleMark.sameAny( integerMark ) );
		System.out.printf( String.valueOf( doubleMark.roundMark() ) );
	}
}

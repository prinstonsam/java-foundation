package com.epam.comparator;

import java.util.Comparator;

/**
 * Created by isamsonov on 3/28/16.
 */
public class GeometricObjectComparator implements Comparator<GeometricObject>{
	@Override
	public int compare( GeometricObject o1, GeometricObject o2 ) {

		return o1.getArea()>o2.getArea() ? -1 : (o1.getArea()<o2.getArea() ? 1 : 0);
	}
}

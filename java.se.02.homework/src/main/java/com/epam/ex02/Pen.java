package com.epam.ex02;

/**
 * Created by isamsonov on 3/29/16.
 */
public class Pen {
	private String type;
	private String color;
	private double cost;

	public Pen(String type, String color, double cost ) {
		this.type = type;
		this.color = color;
		this.cost = cost;

	}

	public Pen() {
	}

	public String getType() {
		return type;
	}

	public void setType( String type ) {
		this.type = type;
	}

	public String getColor() {
		return color;
	}

	public void setColor( String color ) {
		this.color = color;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}



	//Point 8 from Effective java
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Pen pen = (Pen) o;

		if (Double.compare(pen.cost, cost) != 0) return false;
		if (!type.equals(pen.type)) return false;
		return color.equals(pen.color);

	}

	//Potnt 9 from Effective Java
	@Override
	public int hashCode() {
		int result;
		long temp;
		result = type.hashCode();
		result = 31 * result + color.hashCode();
		temp = Double.doubleToLongBits(cost);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}


	//Point 10 from effective java
	@Override
	public String toString() {
		return new StringBuilder("Pen: ")
				.append( "type: " )
				.append( type )
				.append( "color: " )
				.append( color )
				.append( "cost: ")
				.append( cost )
				.toString();
	}
}

package ex02.model;

/**
 * Created by samsik on 25.04.16.
 */
public class StudyTest {
    private String question;
    private String answer;

    public StudyTest(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public StudyTest(String[] questionWithAnswer) {
        this.question = questionWithAnswer[0];
        this.answer = questionWithAnswer[1];
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}

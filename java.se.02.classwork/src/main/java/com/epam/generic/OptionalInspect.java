package com.epam.generic;

/**
 * Created by isamsonov on 3/28/16.
 */
public class OptionalInspect {
	public static void main( String[] args ) {
		Optional<Integer> optional = new Optional<>();
		optional.setValue( 1 );

		int v1 = optional.getValue();
		System.out.println( v1 );

		Optional<String> stringOptional = new Optional<>();
		String v2 = stringOptional.getValue();
		System.out.println( v2 );

		Optional optional1 = new Optional();

		System.out.println( optional1.getValue() );
		optional1.setValue( "Java SE 7" );

		System.out.println(optional1.toString());

		optional1.setValue( 100 );
		System.out.println( optional1.toString() );

		optional1.setValue( null );


	}
}

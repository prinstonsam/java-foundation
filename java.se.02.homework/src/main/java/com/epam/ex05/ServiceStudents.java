package com.epam.ex05;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by isamsonov on 3/31/16.
 */
public class ServiceStudents{
	Set<Student> students;

	public ServiceStudents( Set<Student> students ) {
		if ( this.students == null ) {
			this.students = new HashSet<>();
		} else {
			this.students = students;
		}
	}

	public Student getInstanceStudent( String nameStudent, int id ) {
		Student student = getById( id );

		if ( student == null ) {
			student = new Student( nameStudent, id );
			students.add( student );
		}
		return student;
	}

	Student getById(int id) {
		for ( Student student : students ) {
			if ( id == student.getId() ) {
				return student;
			}
		}
		return null;
	}

	Student getByName(String name) {
		for ( Student student : students ) {
			if ( name.equals( student.getId() ) ) {
				return student;
			}
		}
		return null;
	}

}

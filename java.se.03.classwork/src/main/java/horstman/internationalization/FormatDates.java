package horstman.internationalization;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by isamsonov on 4/4/16.
 */
public class FormatDates {
	public static void main( String[] args ) {

		Locale loc = new Locale( "ru", "RU" );

		DateFormat fmt;
		fmt = DateFormat.getDateInstance( DateFormat.DEFAULT, loc );
		System.out.println( fmt.format( new Date() ) );

		fmt = DateFormat.getDateInstance( DateFormat.FULL, loc );
		System.out.println( fmt.format( new Date() ) );

		fmt = DateFormat.getDateInstance( DateFormat.LONG, loc );
		System.out.println( fmt.format( new Date() ) );

		fmt = DateFormat.getDateInstance( DateFormat.MEDIUM, loc );
		System.out.println( fmt.format( new Date() ) );

		fmt = DateFormat.getDateInstance( DateFormat.SHORT, loc );
		System.out.println( fmt.format( new Date() ) );

		fmt = DateFormat.getTimeInstance( DateFormat.SHORT, loc);
		System.out.println( fmt.format( new Date() ) );

		fmt = DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.SHORT);
		System.out.println( fmt.format( new Date() ) );




	}
}

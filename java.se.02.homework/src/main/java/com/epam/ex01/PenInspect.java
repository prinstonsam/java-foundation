package com.epam.ex01;

/**
* Задание 1. Принципы ООП, простейшие классы и объекты
*
* Разработайте спецификацию и создайте класс Ручка (Pen). Определите в этом классе методы equals(), hashCode() и toString().
*/
public class PenInspect {

	public static void main( String[] args ) {
		Pen pen1 = new Pen("strong", "white");
		Pen pen3 = new Pen("soft", "black");

		System.out.println(pen1);
		System.out.println(pen3);
		System.out.println(pen1.hashCode());
		System.out.println(pen3.hashCode());
		System.out.println(pen1.equals( pen3 ));

		pen1.setType( "soft" );
		pen1.setColor( "black" );
		System.out.println(pen1.hashCode());
		System.out.println(pen3.hashCode());
		System.out.println(pen1.equals( pen3 ));

	}
}

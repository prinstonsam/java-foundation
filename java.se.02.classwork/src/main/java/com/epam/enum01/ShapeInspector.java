package com.epam.enum01;

import sun.security.provider.SHA;

/**
 * Created by isamsonov on 3/29/16.
 */
enum Shape{
	RECTANGEL("red"), TRIANGLE("green"), CIRCLE("blue");
	String color;

	Shape( String color ) {
		this.color = color;
	}

	public String getColor() {
		return color;
	}
}

public class ShapeInspector {
	public static void main( String[] args ) {
		Shape[] shapes = Shape.values();
		for ( Shape shape : shapes ) {
			System.out.println(shape + " " + shape.getColor());
		}
	}

}

package horstman.internationalization.testformats;

import javax.swing.*;
import java.awt.*;

/**
 * Created by isamsonov on 4/4/16.
 */
public class NumberFormatTest {
	public static void main( String[] args ) {
		EventQueue.invokeLater( new Runnable() {
			@Override
			public void run() {
				JFrame frame = new NumberFormatFrame();
				frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
				frame.setVisible( true );
			}
		} );
	}
}

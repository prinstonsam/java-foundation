package com.epam.ex06_07;

@ClassPreamble(
		author = "Igor",
		currentRevision = 1,
		reviewers = {"Igor", "Natalia"}
)
public class SubmarineInspect {
	public static void main( String[] args ) {
		Departure departure = new Departure(
				new ServiceGetSubmarineImpl( ).getShip()
		);

		departure.process();

		NuclearSubmarine submarine = ( NuclearSubmarine ) departure.getShip();

		submarine.setCurrentSpeed( 100 );

		System.out.println(new StringBuilder("submarine : ")
				.append( " departure " )
				.append( "(engine : " )
				.append( submarine.getEngine().getSerialNumber() )
				.append( ")" )
				.append( "current speed : " )
				.append(submarine.getCurrentSpeed())
				.toString());
	}
}

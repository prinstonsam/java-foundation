package com.epam.easyclass;

/**
 * Created by samsik on 27.03.16.
 */
public class Book {
    private int price;

    public Book() {
    }

    public Book(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}

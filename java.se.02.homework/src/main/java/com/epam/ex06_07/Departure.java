package com.epam.ex06_07;

import java.util.Date;

@ClassPreamble(
		author = "Igor",
		currentRevision = 1,
		reviewers = {"Igor", "Natalia"}
)
public class Departure {
	private Ship ship;

	Date departureDate;

	public Departure( Ship ship ) {
		this.ship = ship;
	}

	public void process() {
		this.departureDate = new Date();
		ship.setStatus( Status.DEPARTURE );
	}

	public Ship getShip() {
		return ship;
	}
}

package com.epam.ex05;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by isamsonov on 3/31/16.
 */
public class Task05Inspect {
	public static void main( String[] args ) {
		ServiceDisciplines disciplines = new ServiceDisciplines( null );
		ServiceStudents students = new ServiceStudents( null );

		students.getInstanceStudent( "Igor", 1 );
		students.getInstanceStudent( "Vasja", 2 );
		students.getInstanceStudent( "Petja", 3 );
		students.getInstanceStudent( "Egor", 4 );

		disciplines.getInstanceDiscipline( "Программирование","PR001" );
		disciplines.getInstanceDiscipline( "Операционные системы","OS001" );
		disciplines.getInstanceDiscipline( "Базы данных","DBMS001" );


		GradeStudenInDiscipline gradeStudenInDiscipline01 =
				new GradeStudenInDiscipline(
						students.getInstanceStudent( "Igor", 1 ),
						disciplines.getInstanceDiscipline( "Программирование","PR001" ), 5 );

		GradeStudenInDiscipline gradeStudenInDiscipline02 =
				new GradeStudenInDiscipline(
						students.getInstanceStudent( "Vasja", 2 ),
						disciplines.getInstanceDiscipline( "Программирование","PR001" ), 4 );

		Group group1 = new Group("Group001");
		group1.setDiscipline(disciplines.getInstanceDiscipline("Программирование","PR001"));
		group1.addStudentInDiscipline(gradeStudenInDiscipline01);
		group1.addStudentInDiscipline(gradeStudenInDiscipline02);

		System.out.println(group1.getRankOfStudent(students.getInstanceStudent( "Igor", 1 )));
	}

}

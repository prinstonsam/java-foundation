package com.epam.ex03_ex04;

/**
 * Created by samsik on 30.03.16.
 */
public class OfficeAccessory{
    private final String nameAccessory;
    private double cost;

    public OfficeAccessory(String nameAccessory, double cost) {
        this.nameAccessory = nameAccessory;
        this.cost = cost;
    }

    public String getNameAccessory() {
        return nameAccessory;

    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OfficeAccessory that = (OfficeAccessory) o;

        if (Double.compare(that.cost, cost) != 0) return false;
        return nameAccessory.equals(that.nameAccessory);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = nameAccessory.hashCode();
        temp = Double.doubleToLongBits(cost);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "OfficeAccessory{" +
                "nameAccessory='" + nameAccessory + '\'' +
                '}';
    }

}

package ex03;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

/**
 * Created by samsik on 25.04.16.
 */
public class Main {
    public static void main(String[] args) {
        try(FileInputStream inStream = new FileInputStream("src/main/resources/index.html");
            Scanner scanner = new Scanner(inStream);
        )
        {
            //TODO: first part
            while(scanner.hasNext()){
                scanner.nextLine();
                scanner.findInLine(Pattern.compile("(\\D*files/pic\\d+)(\\d*)(\\D*)"));
                try {
                    MatchResult result = scanner.match();
                    for (int i = 1; i <= result.groupCount(); i++) {
                        System.out.println(result.group(i));
                    }
                } catch (IllegalStateException e) {

                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
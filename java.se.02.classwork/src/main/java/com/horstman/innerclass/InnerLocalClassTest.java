package com.horstman.innerclass;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Created by isamsonov on 3/29/16.
 */
class TalkingClock01 {
	private int interval;
	private boolean beep;

	public TalkingClock01( int interval, boolean beep ) {
		this.interval = interval;
		this.beep = beep;
	}

	public void start(){
		class TimePrinter implements ActionListener {

			@Override
			public void actionPerformed( ActionEvent e ) {
				Date now = new Date();
				System.out.println( "at the tone, the time is " + now );
				if ( beep ) {
					Toolkit.getDefaultToolkit().beep();
				}
			}
		}
		ActionListener listener = new TimePrinter();
		Timer t = new Timer( interval, listener );
		t.start();
	}

}


public class InnerLocalClassTest {
	public static void main( String[] args ) {
		TalkingClock01 clock = new TalkingClock01( 1000, true );
		clock.start();

		JOptionPane.showMessageDialog( null, "Quit program" );

		System.exit( 0 );
	}
}

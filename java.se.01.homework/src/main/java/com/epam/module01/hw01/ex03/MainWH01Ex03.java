package com.epam.module01.hw01.ex03;

import java.util.Scanner;

/**
 * Составить программу для вычисления значений функции F(x) на отрезке [а, b] с шагом h.
 * Результат представить в виде таблицы, первый столбец которой – значения аргумента,
 * второй - соответствующие значения функции: F(x) = tan(2x)-3
 */
public class MainWH01Ex03 {
    public static double func(double x) {
        return (double) (Math.tan(2 * x) - 3);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input low boundary:");
        double low = scanner.nextDouble();

        System.out.println("Input high boundary:");
        double high = scanner.nextDouble();

        System.out.println("Input delta:");
        double delta = scanner.nextDouble();

        System.out.println("x" + "\t" + "F(x)");

        double currentValue = low;
        while (currentValue <= high) {
            System.out.println(currentValue + "\t" + func(currentValue));
            currentValue += delta;
        }
    }
}



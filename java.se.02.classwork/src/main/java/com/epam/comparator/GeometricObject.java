package com.epam.comparator;

/**
 * Created by isamsonov on 3/28/16.
 */
public abstract class GeometricObject {
	public abstract double getArea();
}



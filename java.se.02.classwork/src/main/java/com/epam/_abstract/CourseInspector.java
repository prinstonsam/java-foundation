package com.epam._abstract;

/**
 * use abstract classes
 */
abstract class Course {
    public abstract String getInformation();
}

class BaseCourse extends Course {
    @Override
    public String getInformation() {
        return "Base course";
    }
}

class OptionalCourse extends Course {
    @Override
    public String getInformation() {
        return "Optional course";
    }
}

public class CourseInspector {
    public static void main(String[] args) {
        BaseCourse baseCourse = new BaseCourse();
        OptionalCourse optionalCourse = new OptionalCourse();

        System.out.println(baseCourse.getInformation());
        System.out.println(optionalCourse.getInformation());
    }
}

package com.epam.module01.hw01.ex02;

/**
 * Найти наименьший номер элемента последовательности, для которого выполняется условие M.
 * Вывести на экран этот номер и все элементы ai  где i = 1, 2, ..., п.
 */

import java.util.Scanner;

public class MainHW01Ex02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double e = scanner.nextDouble();

        double result;
        int i;

        for (i = 0;; i++) {
            result = 1/Math.pow(i+1, 2);
            if (result < e) {
                break;
            }
        }

        System.out.println("Result: " + result);
        System.out.println("Index: " + i);
    }
}

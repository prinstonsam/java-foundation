package com.epam.inheritence.override;

/**
 * Created by samsik on 28.03.16.
 */
class MedicalStaff {
    public void info() {
        System.out.println("MedicalStaff");
    }
}

class Doctor extends MedicalStaff {
    public void info() {
        System.out.println("Doctor");
    }
}

public class Hospital {
    public static void main(String[] args) {
        Doctor doctor = new Doctor();
        doctor.info();

        MedicalStaff med = new Doctor();
        med.info();
    }
}

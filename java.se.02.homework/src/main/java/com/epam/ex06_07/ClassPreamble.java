package com.epam.ex06_07;

import java.lang.annotation.Documented;

@Documented
public @interface ClassPreamble {
	String author() default "Igor Samsonov";
	int currentRevision() default  6;
	String[] reviewers() default {"Igor Samsonov"};
}

# Class Object
## Переопределение метода equals
При переопределении необходимо, чтобы выполнялись следующие соглашения:
* рефлексивность
* симметричность
* транзитивность
* непротиворечивость
* ненулевая ссылка
### Рефлексивность
Пример, где нарушается:

```java
public class CaseInsensitiveString {
    private String s;

    public CaseInsensitiveString(String s) {
        if (s == null)
            throw new NullPointerException();
        this.s = s;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CaseInsensitiveString) {
            return s.equalsIgnoreCase(
                    ((CaseInsensitiveString) obj).s);
            if (obj instanceof String) //одностороннее взаимодействие
                return false;
        }
    }
    ...
}
```
```java

```
package com.epam.compareto;

import java.util.Arrays;

/**
 * Created by isamsonov on 3/28/16.
 */
public class PersonInspect {

	public static void main( String[] args ) {
		Person person1 = new Person( "Igor", "Samsonov", 41 );
		Person person2 = new Person( "Nataliha", "Samsonova", 38 );

		Person [] persons = new Person[] {person1, person2};

		Arrays.sort( persons );

		System.out.println(Arrays.toString( persons ));

	}
}

package com.epam.classandobject;

/**
 * Created by samsik on 27.03.16.
 */
public class SimpleSingletonInspect {
    public static void main(String[] args) {
        SimpleSingleton singleton1 = SimpleSingleton.getInstance();
        SimpleSingleton singleton2 = SimpleSingleton.getInstance();

        System.out.println(singleton1);
        System.out.println(singleton2);

    }
}

package com.epam.ex02;

import com.epam.ex02.Pen;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by student on 29/03/16.
 */
public class TestOfficeSupplies {

    Employee employee = new Employee("Igor", new OfficeSupplies() {{
        this.addElement(new Pen("1", "1", 100D));
        this.addElement(new Pen("1", "1", 100D));
        this.addElement(new Pen("1", "1", 100D));
    }});

    @Test
    public void testCalculateCostSupplies(){
        Assert.assertEquals(300, employee.getSupplies().calculateCost(), 1e-15);
    }
}

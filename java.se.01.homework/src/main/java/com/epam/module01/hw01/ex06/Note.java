package com.epam.module01.hw01.ex06;

public class Note {

	private String name;

	private NoteRecord[] records;

	public Note() {
		records = new NoteRecord[ 0 ];
	}

	/**
	 * Создание нового блокнота с указанием названия
	 * @param name	имя блокнота
	 */
	public Note( String name ) {
		this();
		this.name = name;
	}

	/**
	 * Создание нового блокнота с указанием названия и набором записей
	 * @param name		имя блокнота
	 * @param records   массив записей
	 */
	public Note( String name, NoteRecord[] records ) {
		this.name = name;
		this.records = records;
	}

	/**
	 * Добавление новой записи в Блокнот
	 * @param record	запись
	 */
	public void addRecord( NoteRecord record ) {
		NoteRecord[] newRecords = new NoteRecord[ records.length + 1 ];

		System.arraycopy( records, 0, newRecords, 0, records.length );

		newRecords[ newRecords.length - 1 ] = record;

		records = newRecords;
	}

	/**
	 * Удаление записи в блоноте. Массив записей копируется в новый массив.
	 * Если будут ограничения по памяти, то можно обойтись исходным массивом и добавить переменную
	 * размера массива записей
	 *
	 * @param record удаляемая запись
	 */
	public void removeRecord( NoteRecord record ) {
		int index = indexOf( record );

		if ( index != - 1 ) {
			NoteRecord[] newRecords = new NoteRecord[ records.length - 1 ];
			if ( newRecords.length > 0 ) {
				System.arraycopy( records, 0, newRecords, 0,
						index );
				if ( index < records.length - 1 ) {
					System.arraycopy( records, index + 1, newRecords, index,
							records.length - index -1 );
				}
			}
			records = newRecords;
		} else {
			System.out.println("Record not found");
		}
	}

	/**
	 * Модификация записи в блоноте
	 * @param oldRecord
	 * @param newRecord
	 */
	public void modifyRecord( NoteRecord oldRecord, NoteRecord newRecord ) {
		int index = indexOf( oldRecord );
		if ( index > 0 ) {
			records[ index ] = newRecord;
		} else {
			System.out.println("Record not found");
		}
	}

	/**
	 * Поиск элемента в массиве записей. Если найден: возвращается индекс, иначе -1
	 * @param record
	 * @return
	 */
	protected int indexOf( NoteRecord record ) {
		for ( int i = 0; i < records.length; i++ ) {
			if ( record.equals( records[ i ] ) ) {
				return i;
			}
		}
		return - 1;
	}

	/**
	 * Вывод на экран всех записей блокнота
	 */
	public void viewAllResrods() {

		for ( NoteRecord record : records ) {
			System.out.println( record.toString() );
		}
	}


}

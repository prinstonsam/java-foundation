package ex03_ex04;

import com.epam.ex03_ex04.NewbieKit;
import com.epam.ex03_ex04.OfficeAccessory;
import com.epam.ex03_ex04.Pen;
import com.epam.ex03_ex04.Pencil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by samsik on 30.03.16.
 */
public class TestNewbieKit {

    NewbieKit kit;

    final OfficeAccessory[] accessoriesByName = new OfficeAccessory[]{
            new Pen("Pen01", 20D, "red"),
            new Pen("Pen02", 30D, "red"),
            new Pen("Pen02", 40D, "red"),
            new Pen("Pen03", 10D, "red"),
            new Pencil("Pencil01", 20D, 0.7D),
            new Pencil("Pencil02", 30D, 0.7D),
            new Pencil("Pencil02", 40D, 0.7D),
            new Pencil("Pencil03", 10D, 0.7D)
    };

    final OfficeAccessory[] accessoriesByCost = new OfficeAccessory[]{
            new Pen("Pen03", 10D, "red"),
            new Pencil("Pencil03", 10D, 0.7D),
            new Pen("Pen01", 20D, "red"),
            new Pencil("Pencil01", 20D, 0.7D),
            new Pen("Pen02", 30D, "red"),
            new Pencil("Pencil02", 30D, 0.7D),
            new Pen("Pen02", 40D, "red"),
            new Pencil("Pencil02", 40D, 0.7D)
    };

    final OfficeAccessory[] accessoriesByCostAndName = new OfficeAccessory[]{
            new Pen("Pen03", 10D, "red"),
            new Pencil("Pencil03", 10D, 0.7D),
            new Pen("Pen01", 20D, "red"),
            new Pencil("Pencil01", 20D, 0.7D),
            new Pen("Pen02", 30D, "red"),
            new Pencil("Pencil02", 30D, 0.7D),
            new Pen("Pen02", 40D, "red"),
            new Pencil("Pencil02", 40D, 0.7D)
    };

    @Before
    public void init() {
        kit = new NewbieKit();

        kit.addAccessory(new Pen("Pen03", 10D, "red"));
        kit.addAccessory(new Pen("Pen01", 20D, "red"));
        kit.addAccessory(new Pen("Pen02", 30D, "red"));
        kit.addAccessory(new Pen("Pen02", 40D, "red"));
        kit.addAccessory(new Pencil("Pencil03", 10D, 0.7D));
        kit.addAccessory(new Pencil("Pencil01", 20D, 0.7D));
        kit.addAccessory(new Pencil("Pencil02", 30D, 0.7D));
        kit.addAccessory(new Pencil("Pencil02", 40D, 0.7D));
    }


    @Test
    public void testSortByName() throws Exception {
        kit.sortByName();
        Assert.assertArrayEquals(
                accessoriesByName,
                kit.getAccessories().toArray(new OfficeAccessory[kit.getAccessories().size()])
        );
    }

    @Test
    public void testSortByCost() throws Exception {
        kit.sortByCost();
        Assert.assertArrayEquals(
                accessoriesByCost,
                kit.getAccessories().toArray(new OfficeAccessory[kit.getAccessories().size()])
        );
    }

    @Test
    public void testSortByNameCost() throws Exception {
        kit.sortByCostAndName();
        Assert.assertArrayEquals(
                accessoriesByCostAndName,
                kit.getAccessories().toArray(new OfficeAccessory[kit.getAccessories().size()])
        );

    }
}

package ex02.Dao.interfaces;

import ex02.model.StudyTest;

import java.util.List;

/**
 * Created by samsik on 25.04.16.
 */
public interface DaoStudyTests {

    public StudyTest get(String key);

    public List<StudyTest> getAll();

}

package com.epam.ex01;

/**
 * Created by isamsonov on 3/29/16.
 */
public class Pen {
	private String type;
	private String color;

	public Pen( String type, String color ) {
		this.type = type;
		this.color = color;
	}

	public Pen() {
	}

	public String getType() {
		return type;
	}

	public void setType( String type ) {
		this.type = type;
	}

	public String getColor() {
		return color;
	}

	public void setColor( String color ) {
		this.color = color;
	}

	//Point 8 from Effective java
	@Override
	public boolean equals( Object o ) {
		if ( this == o ) return true;
		if ( ! ( o instanceof Pen ) ) return false;

		Pen pen = ( Pen ) o;

		if (hashCode() != o.hashCode()) return false;

		if ( ! getType().equals( pen.getType() ) ) return false;
		return getColor().equals( pen.getColor() );

	}

	//Potnt 9 from Effective Java
	@Override
	public int hashCode() {
		int result = getType().hashCode();
		result = 31 * result + getColor().hashCode();
		return result;
	}

	//Point 10 from effective java
	@Override
	public String toString() {
		return new StringBuilder("Pen: ")
				.append( "type: " )
				.append( type )
				.append( "color: " )
				.append( color )
				.toString();
	}
}

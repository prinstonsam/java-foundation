package horstman.internationalization;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Created by isamsonov on 4/4/16.
 */
public class FormatNumbers {
	public static void main( String[] args ) {
		Locale locale = new Locale( "ru", "RU" );
		NumberFormat currencyInstance = NumberFormat.getCurrencyInstance( locale );
		double amt = 123456.78;
		String format = currencyInstance.format( amt );
		System.out.println(format); //123 456,78 руб.

		locale = new Locale( "de", "DE" );
		currencyInstance = NumberFormat.getCurrencyInstance( locale );
		format = currencyInstance.format( amt );
		System.out.println(format); //123.456,78 €

		//come back
		try {
			Number parse = currencyInstance.parse( format );
			double x  = parse.doubleValue();
			System.out.println(x);
		} catch ( ParseException e ) {
			e.printStackTrace();
		}

	}
}

package com.epam.ex05;

/**
 * Created by isamsonov on 3/31/16.
 */
public class Student {
	private String name;
	private int id;

	public Student( String name, int id ) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId( int id ) {
		this.id = id;
	}

	@Override
	public boolean equals( Object o ) {
		if ( this == o ) return true;
		if ( ! ( o instanceof Student ) ) return false;

		Student student = ( Student ) o;

		if ( getId() != student.getId() ) return false;
		return getName().equals( student.getName() );

	}

	@Override
	public int hashCode() {
		int result = getName().hashCode();
		result = 31 * result + getId();
		return result;
	}

	@Override
	public String toString() {
		return "Student{" +
				"name='" + name + '\'' +
				", id=" + id +
				'}';
	}
}

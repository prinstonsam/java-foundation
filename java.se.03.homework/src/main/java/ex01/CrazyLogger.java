package ex01;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.regex.Pattern;

/**
 * Created by samsik on 25.04.16.
 */
public class CrazyLogger {
    private StringBuilder log;
    private final String logFormat = "%1$td-%1$tm-%1$tY:%1$tH-%1$tM - "; //dd-mm-YYYY : hh-mm – message
    private Formatter formatter;

    public CrazyLogger() {
        formatter = new Formatter();
        log = new StringBuilder();
    }

    private String getFormatingCurrentDateTime(){
        return formatter.format(logFormat, Calendar.getInstance()).toString();
    }

    public void put(String message){
        log.append(getFormatingCurrentDateTime())
                .append(message)
                .append("\n");
    }

    public String find(String pattern){
        //TODO using array string or maybe complex regular expression
        return Pattern.compile(pattern).matcher(log.toString()).toString();

    }

    public static void main(String[] args) {
        CrazyLogger crazyLogger = new CrazyLogger();
        Formatter formatter = new Formatter();
        System.out.println(formatter.format(crazyLogger.logFormat, Calendar.getInstance()));
    }
}

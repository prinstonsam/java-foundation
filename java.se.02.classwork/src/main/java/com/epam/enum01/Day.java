package com.epam.enum01;

/**
 * Created by isamsonov on 3/29/16.
 */
public enum Day {
	SUNDAY, MONDAY, TUESDAY, WENDESDAY, THURSDAY, FRIDAY, SATURDAY;

	public boolean isWeekEnd(){
		switch ( this ) {
			case SUNDAY:
			case SATURDAY:
				return true;
			default:
				return false;
		}
	}
}

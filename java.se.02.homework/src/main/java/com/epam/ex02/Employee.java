package com.epam.ex02;

/**
 Задание 2. Классы и объекты

 Напишите приложение, позволяющее вести учет канцелярских товаров на рабочем месте сотрудника.
 Определите полную стоимость канцтоваров у определенного сотрудника.
 */
public class Employee {
    private String name;
    private OfficeSupplies supplies = new OfficeSupplies();

    public Employee(String name, OfficeSupplies supplies) {
        this.name = name;
        this.supplies = supplies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OfficeSupplies getSupplies() {
        return supplies;
    }

    public void setSupplies(OfficeSupplies supplies) {
        this.supplies = supplies;
    }
}

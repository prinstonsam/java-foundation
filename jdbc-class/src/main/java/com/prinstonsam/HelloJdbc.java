package com.prinstonsam;

import java.sql.*;

/**
 * Created by samsik on 19.03.16.
 */
public class HelloJdbc {
    public static void main(String[] args) {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/mydb", "postgres", "dragutin");
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("select * from type_body");) {

            while (rs.next()) {
                System.out.println(rs.getInt(1) + " " + rs.getString(2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package com.epam.ex06_07;

@ClassPreamble(
		author = "Igor",
		currentRevision = 1,
		reviewers = {"Igor", "Natalia"}
)
public class NuclearSubmarine extends Ship{

	public NuclearSubmarine( String serialNumber ) {
		super(serialNumber);
	}

	private Engine engine;

	public Engine getEngine() {
		return engine;
	}

	public int getCurrentSpeed() {
		return currentSpeed;
	}

	public void setCurrentSpeed( int currentSpeed ) {
		this.currentSpeed = currentSpeed;
	}

	private int currentSpeed;

	public void newEngine( String serialNumber, float power)
	{
		engine = new Engine( serialNumber, power );
	}



	class Engine{
		private String serialNumber;
		private float power;

		public Engine( String serialNumber, float power ) {
			this.serialNumber = serialNumber;
			this.power = power;
		}

		public String getSerialNumber() {
			return serialNumber;
		}

		public void setSerialNumber( String serialNumber ) {
			this.serialNumber = serialNumber;
		}

		public float getPower() {
			return power;
		}

		public void setPower( float power ) {
			this.power = power;
		}

		public void setCurrentSpeed( int value ) {
			NuclearSubmarine.this.setCurrentSpeed( value );
		}
	}
}

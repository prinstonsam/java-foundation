package com.prinstonsam.connectionpool;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by samsik on 19.03.16.
 */
public class ConnectionPool {

    private String driverName;
    private String url;
    private String userName;
    private String password;
    //TODO: properties

    private ConnectionPool() {
        Properties properties = new Properties();

        try {
            properties.load( new FileInputStream( "src/main/resources/db.properties" ) );
        }
        catch ( IOException e ) {
            e.printStackTrace();
        }

        driverName = properties.getProperty( "db.driver" );
        //TODO: load properties


    }
}

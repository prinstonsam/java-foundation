package com.epam.classandobject;

/**
 * From effective java. Problem in equals
 */
class CaseInsensitiveString1 {
    private String s;

    public CaseInsensitiveString1(String s) {
        if (s == null)
            throw new NullPointerException();
        this.s = s;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof CaseInsensitiveString &&  ((CaseInsensitiveString1) obj).s.equalsIgnoreCase(s);
    }
}

public class CaseInsensitiveString{
    public static void main(String[] args) {
        CaseInsensitiveString1 cis = new CaseInsensitiveString1("Polish");
        String s = "polish";
        System.out.println(cis.equals(s));
        System.out.println(s.equals(cis));
    }
}

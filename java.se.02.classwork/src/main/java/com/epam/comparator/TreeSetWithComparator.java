package com.epam.comparator;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by isamsonov on 3/28/16.
 */
public class TreeSetWithComparator {
	public static void main( String[] args ) {
		Comparator<GeometricObject> comparator = new GeometricObjectComparator();
		Set<GeometricObject> set = new TreeSet<>( comparator );
		set.add( new RectangleG0( 4, 5 ) );
		set.add( new CircleG0( 5 ) );
		set.add( new CircleG0( 5 ) );
		set.add( new RectangleG0( 4, 1 ) );
		for ( GeometricObject geometricObject : set ) {
			System.out.println( "area = " + geometricObject.getArea() );
		}
	}
}

package strings;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Locale;

/**
 * Created by isamsonov on 4/1/16.
 */
public class StringInspect {
	public static void main( String[] args ) {

		Locale [] supportedLocales = DateFormat.getAvailableLocales();

		System.out.println( Arrays.toString(supportedLocales));

		Locale locale = new Locale( "ru", "RU" );

		System.out.println(locale.getDisplayName());


	}
}

package strings;

/**
 * Created by isamsonov on 4/1/16.
 */
class A1 implements Cloneable{
	private int fieldA1 = 100;
	private Double fieldDoubleA1 = 100D;

	public int getFieldA1() {
		return fieldA1;
	}

	public void setFieldA1( int fieldA1 ) {
		this.fieldA1 = fieldA1;
	}

	public Double getFieldDoubleA1() {
		return fieldDoubleA1;
	}

	public void setFieldDoubleA1( Double fieldDoubleA1 ) {
		this.fieldDoubleA1 = fieldDoubleA1;
	}

	@Override
	public A1 clone() throws CloneNotSupportedException {
		A1 a1 = (A1)super.clone();
		a1.fieldDoubleA1 = new Double(this.fieldDoubleA1.doubleValue() );
		return a1;
	}
}

class A2 implements Cloneable{
	public A1 a1 = new A1();

	@Override
	public A2 clone() throws CloneNotSupportedException {
		A2 a2 = (A2)super.clone();
		a2.a1 = this.a1.clone();
		return a2;
	}
}

public class InheritanceInspect {
	public static void main( String[] args ) throws CloneNotSupportedException {
		A2 a2 = new A2();

		A2 a2Clone = a2.clone();

		System.out.println(a2Clone.a1.getFieldA1());
		System.out.println(a2Clone.a1.getFieldDoubleA1());

		a2.a1.setFieldDoubleA1( 200D );

		System.out.println(a2Clone.a1.getFieldA1());
		System.out.println(a2Clone.a1.getFieldDoubleA1());
		String s[] = new String[Integer.MAX_VALUE];
		int i =1;

		while ( true ) {
			try{
				s[i]+=s[i-1]+ "fasdsasadsaDSADsaDSAsafddsafsafsadfdsafdsafdsafsadfdsafsadf";
				i++;
			}
			catch(Throwable e){
				System.out.println(s[i].length());
			}

		}
	}

}

package ex02.Dao.impl;

import ex02.Dao.interfaces.DaoStudyTests;
import ex02.model.StudyTest;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by samsik on 25.04.16.
 */
public class StudyTestsByResourceBundle implements DaoStudyTests{
    private static final String DELIMIER_QUESTION_AND_ANSWER = "~";
    ResourceBundle bundle = ResourceBundle.getBundle("StudyTests");

    public StudyTest get(String key) {
        return new StudyTest(parsePairQuestionAndAnswer(bundle.getString(key)));
    }

    public List<StudyTest> getAll() {
        Enumeration<String> keys = bundle.getKeys();
        ArrayList<StudyTest> result = new ArrayList<>();

        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            result.add(new StudyTest(key, keys.nextElement()));
        }
        return result;
    }

    private String[] parsePairQuestionAndAnswer(String pair){
        return pair.split(DELIMIER_QUESTION_AND_ANSWER);

    }
}

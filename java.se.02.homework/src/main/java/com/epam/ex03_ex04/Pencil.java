package com.epam.ex03_ex04;

/**
 * Created by samsik on 30.03.16.
 */
public class Pencil extends OfficeAccessory{

    private final double thicknessStalePencil;

    public Pencil(String typeAccessory, double cost, double thicknessStalePencil) {
        super(typeAccessory, cost);
        this.thicknessStalePencil = thicknessStalePencil;
    }

    public double getThicknessStalePencil() {
        return thicknessStalePencil;
    }

    public double getThicknessStalePencil(double thicknessStalePencil) {
        return this.thicknessStalePencil;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Pencil pencil = (Pencil) o;

        return Double.compare(pencil.thicknessStalePencil, thicknessStalePencil) == 0;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(thicknessStalePencil);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Pencil{" +
                "nameAccessory='" + getNameAccessory() + '\'' +
                "thicknessStalePencil=" + thicknessStalePencil +
                '}';
    }
}

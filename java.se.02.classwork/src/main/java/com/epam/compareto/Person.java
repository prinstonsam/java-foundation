package com.epam.compareto;

/**
 * comparable
 */
public class Person implements Comparable<Person>{

	private String firstName;
	private String lastName;
	private int age;

	public Person( String firstName, String lastName, int age ) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	public Person( String firstName ) {

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName( String firstName ) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName( String lastName ) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge( int age ) {
		this.age = age;
	}

	@Override
	public int compareTo( Person o ) {
		int anotherAge = o.getAge();
		return this.age - anotherAge;
	}

	@Override
	public String toString() {
		return new StringBuilder( lastName )
				.append( " " )
				.append( firstName )
				.append( " " )
				.append( age )
				.toString();
	}
}

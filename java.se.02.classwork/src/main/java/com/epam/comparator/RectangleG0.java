package com.epam.comparator;

/**
 * Created by isamsonov on 3/28/16.
 */
public class RectangleG0 extends GeometricObject {
	private double sideA;
	private double sideB;

	public RectangleG0( double sideA, double sideB ) {
		this.sideA = sideA;
		this.sideB = sideB;
	}

	@Override
	public double getArea() {
		return sideA * sideB;
	}
}

package com.epam.ex06_07;

@ClassPreamble(
		author = "Igor",
		currentRevision = 1,
		reviewers = {"Igor", "Natalia"}
)
public class ServiceGetSubmarineImpl implements ServiceShip {
	public ServiceGetSubmarineImpl() {
	}

	@Override
	public NuclearSubmarine getShip() {

		NuclearSubmarine submarine = new NuclearSubmarine( "S00001" );

		submarine.newEngine( "E00001", 1000000 );

		return submarine;
	}
}

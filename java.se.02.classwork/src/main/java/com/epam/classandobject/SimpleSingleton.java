package com.epam.classandobject;

/**
 * Created by samsik on 27.03.16.
 */
public class SimpleSingleton {
    private static SimpleSingleton instance;

    private SimpleSingleton() {

    }

    public static SimpleSingleton getInstance() {
        if (null == instance) {
            instance = new SimpleSingleton();
        }
        return instance;
    }
}

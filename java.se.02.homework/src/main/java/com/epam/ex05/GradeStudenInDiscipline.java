package com.epam.ex05;

/**
 * Created by isamsonov on 3/31/16.
 */
public class GradeStudenInDiscipline <T extends Comparable> implements Comparable{

	Student student;
	Discipline discipline;

	private T mark;

	public GradeStudenInDiscipline( Student student, Discipline discipline, T mark ) {
		this.student = student;
		this.discipline = discipline;
		this.mark = mark;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent( Student student ) {
		this.student = student;
	}

	public Discipline getDiscipline() {
		return discipline;
	}

	public void setDiscipline( Discipline discipline ) {
		this.discipline = discipline;
	}

	public T getMark() {
		return mark;
	}

	public void setMark( T mark ) {
		this.mark = mark;
	}

	@Override
	public int compareTo(Object o) {
		return mark.compareTo(((GradeStudenInDiscipline)o).getMark());
	}
}

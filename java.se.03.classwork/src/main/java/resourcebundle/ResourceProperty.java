package resourcebundle;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by samsik on 20.04.16.
 */
public class ResourceProperty {
    private ResourceBundle bundle;
    public ResourceProperty(Locale locale) {
        bundle = ResourceBundle
                .getBundle("MyBundle", locale);
    }
    public String getValue(String key) {
        return bundle.getString(key);
    }
}

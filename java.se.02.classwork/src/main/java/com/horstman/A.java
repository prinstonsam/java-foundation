package com.horstman;

import org.jetbrains.annotations.Nullable;

/**
 * Created by isamsonov on 3/30/16.
 */
class B{
	public Integer field01 = null;
	@Nullable
	public Integer getField01() {
		return field01;
	}

	public void setField01( Integer field01 ) {
		this.field01 = field01;
	}
}

public class A {
	public static void main( String[] args ) {
		B b = new B();

		Integer a = 100 + b.getField01();

		System.out.println(a);


	}
}

package com.epam.inheritence.override2;

/**
 * Created by samsik on 28.03.16.
 */
class MedicalStaff {
    public static void staticMedical() {
        System.out.println("staticMedicalStaff");
    }

    public void prescriptionMedicine() {
        System.out.println("presciriptionMedicine");
    }

    public void info() {
        System.out.println("MedicalStaff");
    }
}

class Doctor extends MedicalStaff {
    public static void staticMedical() {
        System.out.println("staticDoctor");
    }

    public void createMedicine() {
        System.out.println("createMedicine");
    }

    @Override
    public void info() {
        System.out.println("Doctor");
    }

}

public class Hospital {

    public static void main(String[] args) {
        MedicalStaff staff = new Doctor();
        Doctor doctor = new Doctor();

        staff.info();
        staff.prescriptionMedicine();
        staff.staticMedical();

        doctor.info();
        doctor.prescriptionMedicine();
        doctor.createMedicine();
        doctor.staticMedical();
    }
}

package com.epam.ex03_ex04;

/**
 * Created by samsik on 30.03.16.
 */
public class Pen extends OfficeAccessory{
    private final String color;

    public Pen(String typeAccessory, double cost, String color) {
        super(typeAccessory, cost);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Pen pen = (Pen) o;

        return color.equals(pen.color);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + color.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Pen{" +
                "nameAccessory='" + getNameAccessory() + '\'' +
                "color='" + color + '\'' +
                '}';
    }
}

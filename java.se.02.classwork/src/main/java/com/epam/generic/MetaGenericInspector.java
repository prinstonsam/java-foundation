package com.epam.generic;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by isamsonov on 3/29/16.
 */
class MedicalStaff{

}

class Doctor extends MedicalStaff {

}

class HeadDoctor extends Doctor {

}

class Nurse extends MedicalStaff {

}

public class MetaGenericInspector {
	public static void main( String[] args ) {
//	List<? extends Doctor> list1 = new ArrayList<MedicalStaff>();//error

		List<? extends Doctor> list2 = new ArrayList<Doctor>();
		List<? extends Doctor> list3 = new ArrayList<HeadDoctor>();

//	List<? super Doctor> list4 = new ArrayList<HeadDoctor>(); //eror
		List<? super Doctor> list5 = new ArrayList<Doctor>();
		List<? super Doctor> list6 = new ArrayList<MedicalStaff>();
		List<? super Doctor> list7 = new ArrayList<Object>();

//		list7.add( new Object() ); //error
//		list7.add( new MedicalStaff()); //error
		list6.add( new Doctor());
		list6.add( new HeadDoctor());
	}



}

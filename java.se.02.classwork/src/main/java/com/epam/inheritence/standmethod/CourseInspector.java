package com.epam.inheritence.standmethod;

/**
 * stand method
 */
class Course{

}

class BaseCourse extends Course {
}

class CourseHelper {
    public Course getCourse() {
        System.out.println("Course");
        return new Course();
    }
}

class BaseCourseHelper extends CourseHelper{
    public  BaseCourse getCourse() {
        System.out.println("BaseCOurse");
        return new BaseCourse();
    }
}

public class CourseInspector {

    public static void main(String[] args) {
        CourseHelper baseCourseHelper = new BaseCourseHelper();
        Course course = baseCourseHelper.getCourse();
        BaseCourse baseCourse= (BaseCourse) baseCourseHelper.getCourse(); //error without explicit casting

        baseCourseHelper.getCourse();
    }


}

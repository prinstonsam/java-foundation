package com.epam.module01.hw01.ex04;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by isamsonov on 3/25/16.
 */
public class OperateTest {

	@Test
	public void testGenerateNewArrayOddCountElements() throws Exception {
		double testArrayOdd[] = { 60, 60, 60 };
		double srcArrayOdd[] = {10, 20, 30, 40, 50};

		Assert.assertArrayEquals( testArrayOdd, Operate.generateNewArray( srcArrayOdd ), 1e-15 );
	}

	@Test
	public void testGenerateNewArrayEvenCountElements() throws Exception {
		double testArrayEven[] = { 90, 90, 90, 90 };
		double srcArrayEven[] = {10, 20, 30, 40, 50, 60, 70, 80};

		Assert.assertArrayEquals( testArrayEven, Operate.generateNewArray( srcArrayEven ), 1e-15 );
	}

    @Test
    public void testMaxElement() throws Exception {
        double srcArray[] = {10, 20, 30, 40, 50, 60, 70, 80};

        Assert.assertEquals(80, Operate.max(srcArray), 1e-15);
    }

}

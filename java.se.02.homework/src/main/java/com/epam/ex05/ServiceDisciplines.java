package com.epam.ex05;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by isamsonov on 3/31/16.
 */
public class ServiceDisciplines {
	private Set<Discipline> disciplines;

	public Discipline getInstanceDiscipline( String nameDiscipline, String codeDiscipline ) {
		Discipline discipline = getByCodeDiscipline( codeDiscipline );

		if ( discipline == null ) {
			discipline = new Discipline( nameDiscipline, codeDiscipline );
			disciplines.add( discipline );
		}
		return discipline;
	}

	public Discipline getByCodeDiscipline( String codeDiscipline ) {
		for ( Discipline discipline : disciplines ) {
			if ( discipline.getCode().equals( codeDiscipline ) ) {
				return discipline;
			}
		}
		return null;
	}

	public ServiceDisciplines( HashSet<Discipline> disciplines ) {
		if ( this.disciplines == null ) {
			this.disciplines = new HashSet<>();
		} else {
			this.disciplines = disciplines;
		}

	}

	public void addDiscipline( Discipline discipline ) {
		disciplines.add( getInstanceDiscipline( discipline.getName(), discipline.getCode() ) );
	}

	public Set<Discipline> getDisciplines() {
		return disciplines;
	}
}

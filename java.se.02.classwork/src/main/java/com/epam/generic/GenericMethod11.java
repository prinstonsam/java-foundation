package com.epam.generic;

/**
 * Created by isamsonov on 3/29/16.
 */
public class GenericMethod11 {
	public static <T extends Number> byte asByte( T num ) {
		long n = num.longValue();
		if (n>=-128 && n<=127) return (byte)n;
		return 0;
//		return n>=-128 && n<=127 ? (byte)n : 0;
	}

	public static void main( String[] args ) {
		long ns = System.nanoTime();
		System.out.println( asByte( 100 ) );
		System.out.println( asByte( 7.1234D ) );
		System.out.println(System.nanoTime() - ns);
	}


}

package com.epam.enum01;

import java.util.Arrays;

/**
 * Created by isamsonov on 3/29/16.
 */
public class DayInspect {
	public static void main( String[] args ) {
		System.out.println(Day.MONDAY + " isWeekEnd(): " + Day.MONDAY.isWeekEnd());

		System.out.println( Arrays.toString(Day.values()));

		System.out.println(Day.valueOf( "MONDAY" ));

		System.out.println(Day.MONDAY);

	}
}

package horstman.internationalization;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by isamsonov on 4/4/16.
 */
public class ResourceBundleInspect {

	public static void main( String[] args ) {
		System.out.println( Locale.getDefault() );
		ResourceBundle myBundle = ResourceBundle.getBundle( "MyBundle", Locale.getDefault() );

		System.out.println( myBundle.getString( "name" ) );

		ResourceBundle.getBundle( "MyBundle", Locale.getDefault() );

	}
}

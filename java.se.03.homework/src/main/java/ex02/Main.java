package ex02;

import ex02.Dao.impl.StudyTestsByResourceBundle;
import ex02.model.StudyTest;

/**
 * Created by samsik on 25.04.16.
 */
public class Main {
    public static void main(String[] args) {

        StudyTestsByResourceBundle studyTestsByResourceBundle = new StudyTestsByResourceBundle();

        StudyTest studyTest = studyTestsByResourceBundle.get("q01");
        System.out.println("q: " + studyTest.getQuestion()+ "  answ: " + studyTest.getAnswer());

    }
}

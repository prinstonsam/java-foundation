package com.epam.ex03_ex04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by samsik on 30.03.16.
 */
public class NewbieKit {

	private List<OfficeAccessory> accessories = new ArrayList<>();

	public OfficeAccessory getAccessoryByIndex( int index ) {
		return accessories.get( index );
	}

	public void setAccessories( List<OfficeAccessory> accessories ) {
		this.accessories = accessories;
	}

	public List<OfficeAccessory> getAccessories() {
		return accessories;
	}

	public void addAccessory( OfficeAccessory accessory ) {
		accessories.add( accessory );
	}

	public void sortByName() {
		Comparator<OfficeAccessory> comparator =
				( o1, o2 ) -> o1.getNameAccessory().compareTo( o2.getNameAccessory() );

		Collections.sort( accessories, comparator );
	}

	public void sortByCost() {
		Comparator<OfficeAccessory> comparator =
				( o1, o2 ) -> o1.getCost() < o2.getCost() ? - 1 : ( o1.getCost() > o2.getCost() ? 1 : 0 );

		Collections.sort( accessories, comparator );
	}

	public void sortByCostAndName() {
		Comparator<OfficeAccessory> comparator =
				( o1, o2 ) ->
						o1.getCost() < o2.getCost() ? - 1 :
								( o1.getCost() > o2.getCost()
										? 1
										: o1.getNameAccessory().compareTo( o2.getNameAccessory() ) );

		Collections.sort( accessories, comparator );
	}
}

package com.epam.ex06_07;

import java.util.Date;

@ClassPreamble(
		author = "Igor",
		currentRevision = 1,
		reviewers = {"Igor", "Natalia"}
)
public class Arrival {
	private NuclearSubmarine submarine;

	Date arrivalDate;

	public Arrival( NuclearSubmarine submarine ) {
		this.submarine = submarine;
	}

	public void process() {
		this.arrivalDate = new Date();
		submarine.setStatus( Status.ARRIVED );
	}
}

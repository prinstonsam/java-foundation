package com.epam.ex05;

/**
 * Created by isamsonov on 3/31/16.
 */
public class Discipline {
	private String name;
	private String code;

	public Discipline( String name, String code ) {
		this.name = name;
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode( String code ) {
		this.code = code;
	}

	@Override
	public boolean equals( Object o ) {
		if ( this == o ) return true;
		if ( ! ( o instanceof Discipline ) ) return false;

		Discipline that = ( Discipline ) o;

		if ( ! getName().equals( that.getName() ) ) return false;
		return getCode().equals( that.getCode() );

	}

	@Override
	public int hashCode() {
		int result = getName().hashCode();
		result = 31 * result + getCode().hashCode();
		return result;
	}

	@Override
	public String toString() {
		return "Discipline{" +
				"name='" + name + '\'' +
				", code='" + code + '\'' +
				'}';
	}
}

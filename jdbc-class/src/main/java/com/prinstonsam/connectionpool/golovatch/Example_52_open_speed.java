package com.prinstonsam.connectionpool.golovatch;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * Created by samsik on 20.03.16.
 */

public class Example_52_open_speed {

    public static Properties getProperties(){
        Properties properties = new Properties();

        try {
            properties.load( new FileInputStream( "src/main/resources/db.properties" ) );
        }
        catch ( IOException e ) {
            e.printStackTrace();
        }
        return properties;
    }

    public static void main(String[] args) {
        Properties properties = getProperties();

       try {
            Class.forName( properties.getProperty( "db.driver" ) );
        }
        catch ( ClassNotFoundException e ) {
            e.printStackTrace();
        }

        for (int i = 0; i < 200; i++) {
            long t0 = System.nanoTime();
            long t1 = 0;
            try (Connection conn = DriverManager.getConnection(
                    properties.getProperty( "db.url" ),
                    properties.getProperty( "db.user" ),
                    properties.getProperty( "db.password" ));)
            {
                t1 = System.nanoTime();
            }
            catch ( SQLException e ) {
                e.printStackTrace();
            }

            System.out.printf("%,10d\n", t1 - t0);
        }

    }
}

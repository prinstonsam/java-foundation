package com.horstman.innerclass;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.*;
import javax.swing.Timer;

/**
 * Created by isamsonov on 3/29/16.
 */
class TalkingClock {
	private int interval;
	private boolean beep;

	public TalkingClock( int interval, boolean beep ) {
		this.interval = interval;
		this.beep = beep;
	}

	public void start(){
		ActionListener listener = new TimePrinter();
		Timer t = new Timer( interval, listener );
		t.start();
	}

	public class TimePrinter implements ActionListener {

		@Override
		public void actionPerformed( ActionEvent e ) {
			Date now = new Date();
			System.out.println( "at the tone, the time is " + now );
			if ( beep ) {
				Toolkit.getDefaultToolkit().beep();
			}

		}
	}
}


public class InnerClassTest {
	public static void main( String[] args ) {
		TalkingClock clock = new TalkingClock( 1000, true );
		clock.start();

		JOptionPane.showMessageDialog( null, "Quit program" );

		System.exit( 0 );
	}
}

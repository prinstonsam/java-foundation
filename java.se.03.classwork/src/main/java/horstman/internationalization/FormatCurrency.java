package horstman.internationalization;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

/**
 * Created by isamsonov on 4/4/16.
 */
public class FormatCurrency {
	public static void main( String[] args ) {
		NumberFormat euroFormatter = NumberFormat.getCurrencyInstance( Locale.US );

		euroFormatter.setCurrency( Currency.getInstance( "EUR" ) );

		System.out.println(euroFormatter.format( 123456.78 ));



	}
}

package com.epam.module01.hw01.ex05;

import java.util.Scanner;

/**
 * Задание 5. Двумерные массивы
 * <p>
 * Получить матрицу:
 * 10001
 * 01010
 * 00100
 * 01010
 * 10001
 */
public class MainHW01Ex05 {
	public static void main( String[] args ) {
		Scanner scanner = new Scanner( System.in );

		System.out.println( "Type size of matrix:" );

		int n = scanner.nextInt();

		int[][] array = new int[ n ][ n ];

		for ( int i = 0; i < n; i++ ) {
			for ( int j = 0; j < n; j++ ) {
				if ( i == j ) {
					array[ i ][ j ] = 1;
				} else if ( j == n - i -1 ) {
					array[ i ][ j ] = 1;
				} else {
					array[ i ][ j ] = 0;
				}
			}
		}

		for ( int i = 0; i < n; i++ ) {
			for ( int j = 0; j < n; j++ ) {
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
	}
}

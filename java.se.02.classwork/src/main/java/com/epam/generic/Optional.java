package com.epam.generic;

/**
 * Created by isamsonov on 3/28/16.
 */
public class Optional<T> {
	private T value;

	public Optional() {
	}

	public Optional( T value ) {
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	public void setValue( T value ) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value == null ? null : value.getClass().getName() + " " + value;
	}
}

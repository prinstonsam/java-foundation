package horstman.internationalization;

import javafx.print.Collation;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;

/**
 * Created by isamsonov on 4/4/16.
 */
public class SortInspect {
	public static void main( String[] args ) {
		ArrayList<String> strings = new ArrayList<String>( );
		strings.add( "Aaaaa" );
		strings.add( "Bbbbb" );
		strings.add( "aaaaa" );
		strings.add( "bbbbb" );
		strings.add( "CCCCC" );

		Collections.sort( strings );
		System.out.println( Arrays.toString( strings.toArray() ) ); //[Aaaaa, Bbbbb, CCCCC, aaaaa, bbbbb]

		Locale loc = new Locale( "en", "EN" );
		Collator col = Collator.getInstance( loc );
		System.out.println(col.getStrength());
		col.setStrength( Collator.PRIMARY );
		Collections.sort( strings, col );
		System.out.println( Arrays.toString( strings.toArray() ) ); //[aaaaa, Aaaaa, bbbbb, Bbbbb, CCCCC]
	}
}

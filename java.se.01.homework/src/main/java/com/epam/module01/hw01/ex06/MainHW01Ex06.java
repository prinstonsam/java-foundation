package com.epam.module01.hw01.ex06;

/**
 * Задание 6. Простейшие классы и объекты
 * Спроектировать и разработать классы Запись в блокноте и Блокнот (записи блокнота хранятся в массиве).
 * Реализовать методы: Добавить запись, Удалить запись, Редактировать запись, Посмотреть все записи.
 * Написать для данного кода javadoc-документацию.
 */
public class MainHW01Ex06 {
	public static void main( String[] args ) {
		Note note1 = new Note( "MyNote" );


		for ( int i = 0; i < 10; i++ ) {
			note1.addRecord( new NoteRecord( Double.toString( Math.random() ) ) );
		}

		note1.viewAllResrods();

		Note note2 = new Note( "aaa" );

		for ( int i = 0; i < 10; i++ ) {
			note2.addRecord( new NoteRecord( Integer.toString( i ) ) );
		}

//		note2.removeRecord( new NoteRecord( "9" ) );

		note2.modifyRecord( new NoteRecord( "8" ), new NoteRecord( "100" ) );

		note2.viewAllResrods();
	}
}

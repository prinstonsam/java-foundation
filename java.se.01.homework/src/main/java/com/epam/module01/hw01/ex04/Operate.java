package com.epam.module01.hw01.ex04;

import java.util.Arrays;

/**
 * Created by isamsonov on 3/25/16.
 */
public class Operate {
	public static double[] generateNewArray( double[] array ) {
		int size;

		if ( array.length % 2 > 0 ) {
			size = ( array.length >> 1 ) + 1;
		} else {
			size = ( array.length >> 1 );
	 	}

		double[] result = new double[ size ];

		for ( int i = 0; i < size; i++ ) {
			result[ i ] = array[ i ] + array[ array.length - i - 1 ];
		}

		return result;
	}

	public static double max(double[] array){
		double max = array[0];
		for (int i = 1; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}


}

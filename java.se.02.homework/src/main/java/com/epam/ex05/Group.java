package com.epam.ex05;

import java.util.*;

/**
 * Created by isamsonov on 3/31/16.
 */
public class Group {
	private String name;
	private Discipline discipline;
	private Date startDate;
	private Date endDate;
	private List<GradeStudenInDiscipline> studenInDisciplines;

	public Group( String name, List<GradeStudenInDiscipline> gradeStudenInDisciplines ) {
		this.name = name;
		this.studenInDisciplines = gradeStudenInDisciplines;
	}

	public Group( String name ) {
		this.name = name;
		studenInDisciplines = new ArrayList<>();
	}

	public void sortStudentsByGrade(){
		Collections.sort(studenInDisciplines);
	}

	public GradeStudenInDiscipline getGradeStudenInDisciplineByStudent(Student student){
		for (GradeStudenInDiscipline studenInDiscipline : studenInDisciplines) {
			if (studenInDiscipline.getStudent().equals(student)){
				return studenInDiscipline;
			}
		}
		return null;
	}

	public int getRankOfStudent(Student student){
		sortStudentsByGrade();

		return studenInDisciplines.indexOf(getGradeStudenInDisciplineByStudent(student))+1;
	}

	public void addStudentInDiscipline(GradeStudenInDiscipline studenInDiscipline){
		this.studenInDisciplines.add(studenInDiscipline);
	}

	public Discipline getDiscipline() {
		return discipline;
	}

	public void setDiscipline(Discipline discipline) {
		this.discipline = discipline;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public List<GradeStudenInDiscipline> getStudenInDisciplines() {
		return studenInDisciplines;
	}

	public void setStudenInDisciplines(List<GradeStudenInDiscipline> studenInDisciplines) {
		this.studenInDisciplines = studenInDisciplines;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate( Date startDate ) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate( Date endDate ) {
		this.endDate = endDate;
	}
}

package com.epam.module01.hw01.ex04;

import java.util.Arrays;

/**
 * Created by isamsonov on 3/25/16.
 */
public class MainHW01Ex04 {
	public static void main( String[] args ) {
		double testArrayOdd[] = new double[9];
		for ( int i = 0; i < 9; i++ ) {
			testArrayOdd[i] = Math.random();
		}

		double[] resultArray = Operate.generateNewArray( testArrayOdd );

		System.out.println( Arrays.toString(testArrayOdd));
		System.out.println( Arrays.toString(resultArray));

		System.out.println("Max element in result array: " + Operate.max(resultArray));

	}
}

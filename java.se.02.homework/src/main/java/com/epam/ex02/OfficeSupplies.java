package com.epam.ex02;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 29/03/16.
 */
public class OfficeSupplies {
    private List<Pen> pens = new ArrayList<>();

    public void addElement(Pen pen) {
        pens.add(pen);
    }


    public double calculateCost() {
        double result = 0;
        for (Pen pen : pens) {
            result += pen.getCost();
        }
        return result;
    }
}

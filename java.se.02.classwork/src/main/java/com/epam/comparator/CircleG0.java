package com.epam.comparator;

/**
 * Created by isamsonov on 3/28/16.
 */
public class CircleG0 extends GeometricObject{
	private double radius;

	public CircleG0( double radius ) {
		this.radius = radius;
	}

	@Override
	public double getArea() {
		return 2 * Math.PI * radius * radius;
	}
}

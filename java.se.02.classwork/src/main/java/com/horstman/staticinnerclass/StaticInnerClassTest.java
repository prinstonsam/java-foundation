package com.horstman.staticinnerclass;

/**
 * Created by isamsonov on 3/29/16.
 */
class ArrayAlg {
	public static Pair minmax( Double[] values ) {
		Double min = Double.MAX_VALUE;
		Double max = Double.MIN_VALUE;
		for ( Double value : values ) {
			if ( value < min ) {
				min = value;
			}
			if ( value < max ) {
				max = value;
			}
		}
		return new Pair( min, max );
	}

	public static class Pair {
		private Double firstValue;
		private Double secondValue;

		public Pair( Double firstValue, Double secondValue ) {
			this.firstValue = firstValue;
			this.secondValue = secondValue;
		}

		public double getFirstValue() {
			return firstValue;
		}

		public double getSecondValue() {
			return secondValue;
		}
	}

}

public class StaticInnerClassTest {
	public static void main( String[] args ) {
		Double[] values = new Double[ 10 ];

		for ( int i = 0; i < 10; i++ ) {
			values[i] = new Double( Math.random() );
		}

		System.out.println( "min value = " + ArrayAlg.minmax( values ).getFirstValue() );
	}
}

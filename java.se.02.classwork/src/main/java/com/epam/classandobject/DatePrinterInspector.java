package com.epam.classandobject;

import java.util.Date;

/**
 * Created by samsik on 27.03.16.
 */
public class DatePrinterInspector {
    public static void main(String[] args) {
        DatePrinter datePrinter = new DatePrinter();
        int x = datePrinter.printDate("01.01.2016");
        datePrinter.printDate(new Date());
        datePrinter.printDate(1, 1, 2016);
    }
}
